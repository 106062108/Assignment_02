# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Bonus Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|On-line Multi-play game|10%|N|
|Off-line Multi-play game|5%|N|
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|N|
|Little helper|5%|N|
|Boss|5%|Y|

## Website Detail Description

# 作品網址：https://106062108.gitlab.io/Assignment_02

# Player Operations Description :
1. Player can move using up, down, left, and right keys.
2. Player can emit bullet using spacebar key.
3. Player can use skill using left shift key if the skill power is full.

# Basic Components Description : 
1. Jucify mechanisms : 
	 * Level :
     * There will be 24 rounds of general enemies and 1 round of boss appear orderly by an enemy time schedule, which will be represented as follows.
     *  There're 6 types of enemies. For each types of enemies, it will appear for 4 rounds respectively, and the number appeared will increase.
     * Enemy Time Schedule : (1 frame = 1/60 sec)
	     * Round  1 ~ 4 :
         > Enemy_01 (health: 50, move velocity: 100, attack period: 500 frames, bullet_num: 1, bullet velocity: 200, bullet damage: 10, killed score: 10)
	     * Round  5 ~ 8 :
         > Enemy_02 (health: 50, move velocity: 100, attack period: 250 frames, bullet_num: 1, bullet velocity: 200, bullet damage: 20, killed score: 30)
	     * Round  9 ~ 12 :
         > Enemy_03 (health: 100, move velocity: 200, attack period: 500 frames, bullet_num: 2, bullet velocity: 250, bullet damage: 30, killed score: 50)
	     * Round 13 ~ 16 :
         > Enemy_04 (health: 100, move velocity: 200, attack period: 250 frames, bullet_num: 2, bullet velocity: 250, bullet damage: 40, killed score: 70)
	     * Round 17 ~ 20 :
         > Enemy_05 (health: 200, move velocity: 300, attack period: 500 frames, bullet_num: 3, bullet velocity: 300, bullet damage: 50, killed score: 90)
	     * Round 21 ~ 24 :
         > Enemy_06 (health: 200, move velocity: 300, attack period: 250 frames, bullet_num: 3, bullet velocity: 300, bullet damage: 60, killed score: 110)
	     * Round      25 :
         > Boss (health: 1000, move velocity: 300, attack period: 250 frames, bullet_num: 5, bullet velocity: 350, bullet damage: 100, killed score: 400)
	   * For general enemies, it will only emit bullet forward; for boss, it will emit bullet 360 degrees.
   * Skill :
     * Player can use skill if the skill power (blue power bar) is full, and then player will emit 5 automatic aiming missiles.
     * The skill power will increase by the time goes, and it will also increase when player hit or kill enemies.
2. Animations :
   * There're different animations played while player moves.
3. Particle Systems :
   * There're explosion effects when player or enemies die.
4. Sound effects :
   * There're bgm while playing game, and sound effects of bullet shoot, missile shoot, and explosion.
   * User can modify volumes of bgm and sound effects in the menu.
5. Leaderboard :
   * User can type user name in the menu.
   * User can view learder board in the game win or game lose scene.

# Bonus Functions Description : 
1. Bullet Automatic Aiming :
   * Player can emit automatic aiming missiles using skill.
2. Boss :
   * Boss will appear at round 25, and it's health will increase, attack will be more powerful, and bullet damage will increase.