var label_y_position_set = undefined;
var selected_label_index = undefined;
var label_arrow = undefined;
var volume_change_interval = 0.05;

var menuState = {
    create: function() {
        // display background image
        game.add.image(0, 0, 'menu_background');

        // display title label
        var title_label = game.add.text(game.width*0.5, game.height*0.2, 'Raiden', {font: '60px Arial', fill: '#FFFFFF'});
        title_label.anchor.setTo(0.5, 0.5);
        
        // display score label
        var score_label = game.add.text(game.width*0.5, game.height*0.35, ('Score : ' + game.global.score), {font: '25px Arial', fill: '#FFFFFF'});
        score_label.anchor.setTo(0.5, 0.5);
        
        // display bgm, sound effect controller label
        label_y_position_set = [game.height*0.45, game.height*0.55];
        selected_label_index = 0;

        this.bgm_controller_label = game.add.text(game.width*0.5, label_y_position_set[0], ('BGM : ' + parseInt(game.global.bgm_volume*100) + " %"), {font: '30px Arial', fill: '#FFFFFF'});
        this.bgm_controller_label.anchor.setTo(0.5, 0.5);

        this.sound_effect_controller_label = game.add.text(game.width*0.5, label_y_position_set[1], ('Sound Effect : ' + parseInt(game.global.sound_effect_volume*100) + " %"), {font: '30px Arial', fill: '#FFFFFF'});
        this.sound_effect_controller_label.anchor.setTo(0.5, 0.5);

        // display arrow key label
        var arrow_key_label = game.add.text(game.width*0.5, game.height*0.75, 'Press \'Up, Down, Left, Right\' to Change Volume', {font: '20px Arial', fill: '#FFFFFF'});
        arrow_key_label.anchor.setTo(0.5, 0.5);

        // display enter label
        var enter_label = game.add.text(game.width*0.5, game.height*0.8, 'Press \'Enter\' to Start Game', {font: '20px Arial', fill: '#FFFFFF'});
        enter_label.anchor.setTo(0.5, 0.5);

        // display label arrow
        label_arrow = game.add.sprite(game.width*0.25, label_y_position_set[selected_label_index], 'red_arrow');
        label_arrow.scale.setTo(0.05, 0.05);
        label_arrow.anchor.setTo(0.5, 0.5);

        // add up, down, left, right, enter key
        var up_key = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        up_key.onDown.add(this.key_up);

        var down_key = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        down_key.onDown.add(this.key_down);

        var left_key = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        left_key.onDown.add(this.key_left);

        var right_key = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        right_key.onDown.add(this.key_right);

        var enter_key = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enter_key.onDown.add(this.key_enter, this);

        // add text input
        var text_input_label = game.add.text(game.width*0.4, game.height*0.65, 'User Name : ', {font: '25px Arial', fill: '#FFFFFF'});
        text_input_label.anchor.setTo(0.5, 0.5);

        game.add.plugin(PhaserInput.Plugin);
        var text_input_config = {
            font: '25px Arial',
            fill: '#000000',
            width: 200,
            borderWidth: 1,
            borderColor: '#000000',
            placeHolder: 'User Name',
            type: PhaserInput.InputType.text
        };
        this.text_input = game.add.inputField((game.width-text_input_config.width)*0.655,
                                              (game.height-parseInt(text_input_config.font.split(' ')[0].replace('px', '')))*0.65,
                                              text_input_config);
        if(game.global.user_name != undefined) {
            this.text_input.setText(game.global.user_name);
        }
    },

    update: function() {
        this.bgm_controller_label.text = 'BGM : ' + parseInt(game.global.bgm_volume*100) + " %";
        this.sound_effect_controller_label.text = 'Sound Effect : ' + parseInt(game.global.sound_effect_volume*100) + " %";
    },

    key_up: function() {
        selected_label_index = (selected_label_index == 0) ? 0 : (selected_label_index-1);
        label_arrow.y = label_y_position_set[selected_label_index];
    },

    key_down: function() {
        selected_label_index = (selected_label_index == (label_y_position_set.length-1)) ? (label_y_position_set.length-1) : (selected_label_index+1);
        label_arrow.y = label_y_position_set[selected_label_index];
    },

    key_left: function() {
        switch(selected_label_index) {
            case 0:
                game.global.bgm_volume = ((game.global.bgm_volume - volume_change_interval) <= 0) ? 0 : (parseFloat((game.global.bgm_volume - volume_change_interval).toPrecision(2)));
                break;
            case 1:
                game.global.sound_effect_volume = ((game.global.sound_effect_volume - volume_change_interval) <= 0.0) ? 0.0 : (parseFloat((game.global.sound_effect_volume - volume_change_interval).toPrecision(2)));
                break;
        }
    },

    key_right: function() {
        switch(selected_label_index) {
            case 0:
                game.global.bgm_volume = ((game.global.bgm_volume + volume_change_interval) >= 1) ? 1 : (parseFloat((game.global.bgm_volume + volume_change_interval).toPrecision(2)));
                break;
            case 1:
                game.global.sound_effect_volume = ((game.global.sound_effect_volume + volume_change_interval) >= 1.0) ? 1.0 : (parseFloat((game.global.sound_effect_volume + volume_change_interval).toPrecision(2)));
                break;
        }
    },

    key_enter: function() {
        if(this.text_input.text._text == "") {
            if(game.global.user_name != undefined) {
                this.text_input.setText(game.global.user_name);
            }
            else {
                this.text_input.setText('User');
            }
        }
        else {
            // update user name
            game.global.user_name = this.text_input.text._text;
            
            // start play state
            game.state.start('play');
        }
    }
}