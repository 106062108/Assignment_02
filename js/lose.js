var state_set = ['leader_board', 'play', 'menu'];
var label_y_position_set = undefined;
var selected_label_index = undefined;
var label_arrow = undefined;

var loseState = {
    create: function() {
        // display background image
        game.add.image(0, 0, 'lose_background');

        // display title label
        var title_label = game.add.text(game.width*0.5, game.height*0.2, 'Game Over', {font: '50px Arial', fill: '#FFFFFF'});
        title_label.anchor.setTo(0.5, 0.5);
        
        // display score label
        var score_label = game.add.text(game.width*0.5, game.height*0.35, ('Score : ' + game.global.score), {font: '25px Arial', fill: '#FFFFFF'});
        score_label.anchor.setTo(0.5, 0.5);
        
        // display leader board, restart, menu, enter label
        label_y_position_set = [game.height*0.45, game.height*0.55, game.height*0.65];
        selected_label_index = 0;

        var leader_board_label = game.add.text(game.width*0.5, label_y_position_set[0], 'View Leader Board', {font: '30px Arial', fill: '#FFFFFF'});
        leader_board_label.anchor.setTo(0.5, 0.5);
        
        var restart_label = game.add.text(game.width*0.5, label_y_position_set[1], 'Restart Game', {font: '30px Arial', fill: '#FFFFFF'});
        restart_label.anchor.setTo(0.5, 0.5);

        var menu_label = game.add.text(game.width*0.5, label_y_position_set[2], 'Back to Menu', {font: '30px Arial', fill: '#FFFFFF'});
        menu_label.anchor.setTo(0.5, 0.5);

        var enter_label = game.add.text(game.width*0.5, game.height*0.8, 'Press \'Enter\' to Confirm', {font: '20px Arial', fill: '#FFFFFF'});
        enter_label.anchor.setTo(0.5, 0.5);

        var enter_label = game.add.text(game.width*0.5, game.height*0.8, 'Press \'Enter\' to Confirm', {font: '20px Arial', fill: '#FFFFFF'});
        enter_label.anchor.setTo(0.5, 0.5);

        // display label arrow
        label_arrow = game.add.sprite(game.width*0.25, label_y_position_set[selected_label_index], 'red_arrow');
        label_arrow.scale.setTo(0.05, 0.05);
        label_arrow.anchor.setTo(0.5, 0.5);

        // add up, down, enter key
        var up_key = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        up_key.onDown.add(this.key_up);

        var down_key = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        down_key.onDown.add(this.key_down);

        var enter_key = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enter_key.onDown.add(this.key_enter);
    },

    key_up: function() {
        selected_label_index = (selected_label_index == 0) ? 0 : (selected_label_index-1);
        label_arrow.y = label_y_position_set[selected_label_index];
    },

    key_down: function() {
        selected_label_index = (selected_label_index == (label_y_position_set.length-1)) ? (label_y_position_set.length-1) : (selected_label_index+1);
        label_arrow.y = label_y_position_set[selected_label_index];
    },

    key_enter: function() {
        game.state.start(state_set[selected_label_index]);
    }
}