var leaderBoardState = {
    create: function() {
        // display background image
        game.add.image(0, 0, 'leader_board_background');

        // display title label
        var title_label = game.add.text(game.width*0.5, game.height*0.1, 'Leader Board', {font: '50px Arial', fill: '#FFFFFF'});
        title_label.anchor.setTo(0.5, 0.5);

        // display record list
        var label_y_position_set = [game.height*0.25, game.height*0.35, game.height*0.45, game.height*0.55, game.height*0.65];
        for(var i = 0; i < Math.min(game.global.record_list.length, label_y_position_set.length); i++) {
            var textFill = (game.global.user_name == game.global.record_list[i].user_name) ? '#FFFF00' : '#FFFFFF';
            var new_record = {
                user_name_label: game.add.text(game.width*0.3, label_y_position_set[i], game.global.record_list[i].user_name, {font: '30px Arial', fill: textFill}),
                score_label: game.add.text(game.width*0.7, label_y_position_set[i], game.global.record_list[i].score, {font: '30px Arial', fill: textFill})
            };

            new_record.user_name_label.anchor.setTo(0.5, 0.5);
            new_record.score_label.anchor.setTo(0.5, 0.5);
        }

        // display enter label
        var enter_label = game.add.text(game.width*0.5, game.height*0.8, 'Press \'Enter\' to Back to Menu', {font: '20px Arial', fill: '#FFFFFF'});
        enter_label.anchor.setTo(0.5, 0.5);

        // add enter key
        var enter_key = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enter_key.onDown.add(this.key_enter, this);
    },

    key_enter: function() {
        // start menu state
        game.state.start('menu');
    }
}