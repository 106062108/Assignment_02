var loadState = {
    preload: function() {
        // display loading label
        var loading_label = game.add.text(game.width*0.5, game.height*0.4, 'loading...', {font: '30px Arial', fill: '#FFFFFF'});
        loading_label.anchor.setTo(0.5, 0.5);

        // display progress bar
        var progress_bar = game.add.sprite(game.width*0.5, game.height*0.6, 'progress_bar');
        progress_bar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progress_bar);

        // load background image
        game.load.image('menu_background', 'assets/menu_background.png');
        game.load.image('play_background', 'assets/play_background.png');
        game.load.image('win_background', 'assets/win_background.png');
        game.load.image('lose_background', 'assets/lose_background.png');
        game.load.image('leader_board_background', 'assets/leader_board_background.png');

        // load arrow
        game.load.image('red_arrow', 'assets/red_arrow.png');

        // load player sprite sheet
        game.load.spritesheet('player', 'assets/player.png', 91, 122);

        // load fighter image
        game.load.image('fighter_01', 'assets/fighter_01.png');
        game.load.image('fighter_02', 'assets/fighter_02.png');
        game.load.image('fighter_03', 'assets/fighter_03.png');
        game.load.image('fighter_04', 'assets/fighter_04.png');
        game.load.image('fighter_05', 'assets/fighter_05.png');
        game.load.image('fighter_06', 'assets/fighter_06.png');

        // load boss image
        game.load.image('boss', 'assets/boss.png');

        // load bullet image
        game.load.image('red_bullet', 'assets/red_bullet.png');
        game.load.image('orange_bullet', 'assets/orange_bullet.png');
        game.load.image('yellow_bullet', 'assets/yellow_bullet.png');
        game.load.image('green_bullet', 'assets/green_bullet.png');
        game.load.image('blue_bullet', 'assets/blue_bullet.png');
        game.load.image('sky_blue_bullet', 'assets/sky_blue_bullet.png');
        game.load.image('purple_bullet', 'assets/purple_bullet.png');
        game.load.image('pink_bullet', 'assets/pink_bullet.png');

        // load missile image
        game.load.image('red_missile', 'assets/red_missile.png');

        // load emitter image
        game.load.image('explode_emitter', 'assets/explode_emitter.png');

        // load bgm
        game.load.audio('play_bgm', ['assets/play_bgm.mp3']);

        // load sound effect
        game.load.audio('bullet_shoot_sound_effect', ['assets/bullet_shoot_sound_effect.wav']);
        game.load.audio('missile_shoot_sound_effect', ['assets/missile_shoot_sound_effect.wav']);
        game.load.audio('explode_sound_effect', ['assets/explode_sound_effect.wav']);

        // load score list
        firebase.database().ref('record_list').once('value').then(function(snapshot) {
            snapshot.forEach(function(childsnapshot) {
                var new_record = {
                    user_name: childsnapshot.key,
                    score: childsnapshot.val().score
                };

                game.global.record_list.push(new_record);
            });
        });
    },

    create: function() {
        // start menu state
        game.state.start('menu');
    }
}