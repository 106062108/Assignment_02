// initialize Phaser
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'Phaser-Game');

// add global
game.global = {
    user_name: undefined,
    score: 0,
    bgm_volume: 0.50,
    sound_effect_volume: 0.10,
    record_list: []
};

// add state
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('win', winState);
game.state.add('lose', loseState);
game.state.add('leader_board', leaderBoardState);

// start boot state
game.state.start('boot');