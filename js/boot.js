var bootState = {
    preload: function() {
        // load progress bar image
        game.load.image('progress_bar', 'assets/progress_bar.png');
    },

    create: function() {
        // set some game setting
        game.stage.backgroundColor = '#3498db';
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // start load state
        game.state.start('load');
    }
}