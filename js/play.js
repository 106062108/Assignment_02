var refresh_frequency = 60;
var background_scroll_velocity = {
    x: 0,
    y: 100
};
var information_bar_height = 50;
var enemy_idle_distance = 30;
var max_enemy_bullet_num = 100;

/*
Attention about "angle" :
        -y
       (270*)
         |
         |
         |
 -x ----------- +x
(180*)   |    (0*)
         |
         |
        +y
       (90*)
*/

var Player = {
    max_health: 5000,

    max_power: 100,
    power_increment: {
        timer: 1,
        attack: 2,
        kill: 5
    },
    
    player_imageName: 'player',
    move_velocity: {
        x: 300,
        y: 300
    },

    attack_period: 10,

    bullet_imageName: 'purple_bullet',
    bullet_velocity: 200,
    bullet_angle: 270,
    bullet_damage: 10,

    missile_imageName: 'red_missile',
    missile_num: 5,
    missile_emit_interval: 30,
    missile_velocity: 400,
    missile_angle: 270,
    missile_damage: 50
};

var Enemy_01 = {
    max_health: 50,
    
    enemy_imageName: 'fighter_01',
    
    move_period: 30,

    move_velocity: 100,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 500,

    bullet_imageName: 'sky_blue_bullet',
    bullet_num: 1,
    bullet_emit_interval: 5,
    bullet_velocity: 200,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180],
    bullet_damage: 10,

    killed_score: 10
};

var Enemy_02 = {
    max_health: 50,
    
    enemy_imageName: 'fighter_02',
    
    move_period: 30,

    move_velocity: 100,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 250,

    bullet_imageName: 'blue_bullet',
    bullet_num: 1,
    bullet_emit_interval: 5,
    bullet_velocity: 200,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180],
    bullet_damage: 20,

    killed_score: 30
};

var Enemy_03 = {
    max_health: 100,
    
    enemy_imageName: 'fighter_03',
    
    move_period: 30,

    move_velocity: 200,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 500,

    bullet_imageName: 'green_bullet',
    bullet_num: 2,
    bullet_emit_interval: 5,
    bullet_velocity: 250,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180],
    bullet_damage: 30,

    killed_score: 50
};

var Enemy_04 = {
    max_health: 100,
    
    enemy_imageName: 'fighter_04',
    
    move_period: 30,

    move_velocity: 200,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 250,

    bullet_imageName: 'yellow_bullet',
    bullet_num: 2,
    bullet_emit_interval: 5,
    bullet_velocity: 250,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180],
    bullet_damage: 40,

    killed_score: 70
};

var Enemy_05 = {
    max_health: 200,
    
    enemy_imageName: 'fighter_05',
    
    move_period: 30,

    move_velocity: 300,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 500,

    bullet_imageName: 'orange_bullet',
    bullet_num: 3,
    bullet_emit_interval: 5,
    bullet_velocity: 300,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180],
    bullet_damage: 50,

    killed_score: 90
};

var Enemy_06 = {
    max_health: 200,
    
    enemy_imageName: 'fighter_06',
    
    move_period: 30,

    move_velocity: 300,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 250,

    bullet_imageName: 'pink_bullet',
    bullet_num: 3,
    bullet_emit_interval: 5,
    bullet_velocity: 300,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180],
    bullet_damage: 60,

    killed_score: 110
};

var Boss = {
    max_health: 1000,
    
    enemy_imageName: 'boss',
    
    move_period: 30,

    move_velocity: 300,
    move_angle_set: [0, 45, 90, 135, 180, 225, 270, 315],

    attack_period: 250,

    bullet_imageName: 'red_bullet',
    bullet_num: 5,
    bullet_emit_interval: 5,
    bullet_velocity: 350,
    bullet_angle_set: [0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340],
    bullet_damage: 100,

    killed_score: 400
};

var playState = {
    create: function() {
        // add keyboard input
        this.cursor = game.input.keyboard.createCursorKeys();
        this.keyboard = game.input.keyboard;

        // add pause key
        this.pause_key = game.input.keyboard.addKey(Phaser.Keyboard.P);
        this.pause_key.onDown.add(function() {
            game.paused = !game.paused;
        });

        // add this.pause
        this.pause = false;

        // add timer
        this.timer = 0;
        this.timer_counter = 0;

        // add background
        this.background = game.add.tileSprite(0, 0, game.width, game.height, 'play_background');

        // display score label
        this.score_label = game.add.text(75, 25, ('score : ' + game.global.score), {font: '25px Arial', fill: '#FFFFFF'});
        this.score_label.anchor.setTo(0.5, 0.5);
        this.scoreReset();

        // add bgm
        var bgm_soundEffectName = 'play_bgm';
        var bgm_volume = game.global.bgm_volume;
        var bgm_loop = false;
        this.bgm = this.creat_new_sound_effect(bgm_soundEffectName, bgm_volume, bgm_loop);
        this.bgm.play();
        
        // add player
        var health_bar_config = {
            width: game.width - 100,
            height: 10,
            x: game.width/2,
            y: game.height - 30,
            bg: {
                color: '#000000'
            },
            bar: {
                color: '#FF0000'
            },
            animationDuration: 50,
            flipped: false
        };
        var power_bar_config = {
            width: game.width - 100,
            height: 10,
            x: game.width/2,
            y: game.height - 10,
            bg: {
                color: '#000000'
            },
            bar: {
                color: '#00FF'
            },
            animationDuration: 50,
            flipped: false
        };
        var new_player = {
            sprite: game.add.sprite(game.width*0.5, game.height*0.7, Player.player_imageName),
            max_health: Player.max_health,
            health: Player.max_health,
            health_bar: new HealthBar(game, health_bar_config),
            max_power: Player.max_power,
            power: 0,
            power_increment: Player.power_increment,
            power_bar: new HealthBar(game, power_bar_config),
            move_velocity: Player.move_velocity,
            attack_counter: Player.attack_period,
            attack_period: Player.attack_period,
            bullet_imageName: Player.bullet_imageName,
            bullet_velocity: Player.bullet_velocity,
            bullet_angle: Player.bullet_angle,
            bullet_damage: Player.bullet_damage,
            missile_attack_ongoing_counter: 0,
            missile_attack_ongoing_bullet_set: [],
            missile_imageName: Player.missile_imageName,
            missile_num: Player.missile_num,
            missile_emit_interval: Player.missile_emit_interval,
            missile_velocity: Player.missile_velocity,
            missile_angle: Player.missile_angle,
            missile_damage: Player.missile_damage
        };
        new_player.sprite.scale.setTo(0.55, 0.55);
        new_player.sprite.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(new_player.sprite);

        // add animation on player
        new_player.sprite.animations.add('player_move_right', [0], 8, false);
        new_player.sprite.animations.add('player_move', [4], 8, false);
        new_player.sprite.animations.add('player_move_left', [8], 8, false);

        this.player = new_player;
        this.resetPlayerPower();

        // add enemy create schedule
        
        
        this.enemy_create_schedule = [ {timer: 5,   num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_01}]},
                                       {timer: 15,  num: 2, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_01},
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_01}]},
                                       {timer: 25,  num: 3, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_01},
                                                                                    {position: {x: game.width*0.3, y:game.height*0.3}, Enemy: Enemy_01},
                                                                                    {position: {x: game.width*0.7, y:game.height*0.3}, Enemy: Enemy_01}]},
                                       {timer: 35,  num: 4, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_01}, 
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_01},
                                                                                    {position: {x: game.width*0.2, y:game.height*0.3}, Enemy: Enemy_01},
                                                                                    {position: {x: game.width*0.8, y:game.height*0.3}, Enemy: Enemy_01}]},
                                       {timer: 50,  num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_02}]},
                                       {timer: 60,  num: 2, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_02},
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_02}]},
                                       {timer: 70,  num: 3, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_02},
                                                                                    {position: {x: game.width*0.3, y:game.height*0.3}, Enemy: Enemy_02},
                                                                                    {position: {x: game.width*0.7, y:game.height*0.3}, Enemy: Enemy_02}]},
                                       {timer: 80,  num: 4, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_02}, 
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_02},
                                                                                    {position: {x: game.width*0.2, y:game.height*0.3}, Enemy: Enemy_02},
                                                                                    {position: {x: game.width*0.8, y:game.height*0.3}, Enemy: Enemy_02}]},
                                       {timer: 95,  num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_03}]},
                                       {timer: 105, num: 2, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_03},
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_03}]},
                                       {timer: 115, num: 3, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_03},
                                                                                    {position: {x: game.width*0.3, y:game.height*0.3}, Enemy: Enemy_03},
                                                                                    {position: {x: game.width*0.7, y:game.height*0.3}, Enemy: Enemy_03}]},
                                       {timer: 125, num: 4, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_03}, 
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_03},
                                                                                    {position: {x: game.width*0.2, y:game.height*0.3}, Enemy: Enemy_03},
                                                                                    {position: {x: game.width*0.8, y:game.height*0.3}, Enemy: Enemy_03}]},
                                       {timer: 140, num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_04}]},
                                       {timer: 150, num: 2, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_04},
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_04}]},
                                       {timer: 160, num: 3, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_04},
                                                                                    {position: {x: game.width*0.3, y:game.height*0.3}, Enemy: Enemy_04},
                                                                                    {position: {x: game.width*0.7, y:game.height*0.3}, Enemy: Enemy_04}]},
                                       {timer: 170, num: 4, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_04}, 
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_04},
                                                                                    {position: {x: game.width*0.2, y:game.height*0.3}, Enemy: Enemy_04},
                                                                                    {position: {x: game.width*0.8, y:game.height*0.3}, Enemy: Enemy_04}]},
                                       {timer: 185, num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_05}]},
                                       {timer: 195, num: 2, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_05},
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_05}]},
                                       {timer: 205, num: 3, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_05},
                                                                                    {position: {x: game.width*0.3, y:game.height*0.3}, Enemy: Enemy_05},
                                                                                    {position: {x: game.width*0.7, y:game.height*0.3}, Enemy: Enemy_05}]},
                                       {timer: 215, num: 4, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_05}, 
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_05},
                                                                                    {position: {x: game.width*0.2, y:game.height*0.3}, Enemy: Enemy_05},
                                                                                    {position: {x: game.width*0.8, y:game.height*0.3}, Enemy: Enemy_05}]},
                                       {timer: 230, num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_06}]},
                                       {timer: 240, num: 2, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_06},
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_06}]},
                                       {timer: 250, num: 3, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Enemy_06},
                                                                                    {position: {x: game.width*0.3, y:game.height*0.3}, Enemy: Enemy_06},
                                                                                    {position: {x: game.width*0.7, y:game.height*0.3}, Enemy: Enemy_06}]},
                                       {timer: 260, num: 4, enemy_information_set: [{position: {x: game.width*0.4, y:game.height*0.3}, Enemy: Enemy_06}, 
                                                                                    {position: {x: game.width*0.6, y:game.height*0.3}, Enemy: Enemy_06},
                                                                                    {position: {x: game.width*0.2, y:game.height*0.3}, Enemy: Enemy_06},
                                                                                    {position: {x: game.width*0.8, y:game.height*0.3}, Enemy: Enemy_06}]},
                                       {timer: 275, num: 1, enemy_information_set: [{position: {x: game.width*0.5, y:game.height*0.3}, Enemy: Boss}]}
                                       ];
        
        // add enemy set
        this.enemy_set = [];

        // add bullet set
        this.player_bullet_set = [];
        this.enemy_bullet_set = [];

        // add missile set
        this.player_missile_set = [];
    },

    update: function() {
        if(!this.pause) {
            // update timer
            this.update_timer();

            // update background
            this.background.autoScroll(background_scroll_velocity.x, background_scroll_velocity.y);

            // player action
            this.playerMove();
            this.playerAttack();
            this.playerUltimateSkill();
            this.playerMissileMove();
            this.playerMissileAttackOngoing();

            // detect bullect attack
            this.detect_bullet_attack();
        }

        // create enemy
        this.create_enemy();

        // enemy action
        for(var i = 0; i < this.enemy_set.length; i++) {
            var processing_enemy = this.enemy_set[i];
            this.enemyMove(processing_enemy);
            this.enemyAttack(processing_enemy);
            this.enemyAttackOngoing(processing_enemy);
        }
    },

    // two point distance
    two_point_distance: function(position_1, position_2) {
        return Math.pow((Math.pow((position_1.x - position_2.x), 2) + Math.pow((position_1.y - position_2.y), 2)), 0.5);
    },

    // circle overlap
    circle_overlap: function(object_1, object_2) {
        var position_1 = {x: object_1.x, y: object_1.y};
        var position_2 = {x: object_2.x, y: object_2.y};
        var distance = this.two_point_distance(position_1, position_2);
        if(distance < (object_1.r + object_2.r)) {
            return true;
        }
        else {
            return false;
        }
    },

    // search nearest enemy position
    search_nearest_enemy_position: function(attacker_position, enemy_set) {
        var nearest_enemy_distance = undefined;
        var nearest_enemy_position = undefined;
        for(var i = 0; i < enemy_set.length; i++) {
            if(enemy_set[i].idle) {
                var processing_enemy_position = {
                    x: enemy_set[i].sprite.x,
                    y: enemy_set[i].sprite.y
                };
                var new_enemy_distance = this.two_point_distance(attacker_position, processing_enemy_position);
                if(nearest_enemy_distance == undefined) {
                    nearest_enemy_position = processing_enemy_position;
                }
                else if(new_enemy_distance < nearest_enemy_distance) {
                    nearest_enemy_position = processing_enemy_position;
                }
            }
        }

        return nearest_enemy_position;
    },

    // update timer
    update_timer: function() {
        if(this.timer_counter == refresh_frequency) {
            this.timer++;
            this.timer_counter = 0;
            // console.log("time: " + this.timer);

            // increase player's power using timer
            this.playerPowerIncrementUsingTimer();
        }
        else {
            this.timer_counter++;
        }
    },

    // player's action
    /* using move_acceleration and resistant_acceleration to change player's velocity
    playerMove: function() {
        // move up
        if(this.cursor.up.isDown) {
            if(this.player.sprite.y - this.player.move_acceleration.y < Math.abs(this.player.sprite.height)/2){
                this.player.sprite.y = Math.abs(this.player.sprite.height)/2;
                this.player.sprite.body.velocity.y = 0;
            }
            else {
                this.player.sprite.body.velocity.y -= this.player.move_acceleration.y;
            }
            
        }
        // move down
        else if(this.cursor.down.isDown) { 
            if(this.player.sprite.y + this.player.move_acceleration.y > game.height - information_bar_height - Math.abs(this.player.sprite.height)/2){
                this.player.sprite.y = game.height - information_bar_height - Math.abs(this.player.sprite.height)/2;
                this.player.sprite.body.velocity.y = 0;
            }
            else {
                this.player.sprite.body.velocity.y += this.player.move_acceleration.y;
            }
        }
        // if neither up nor down is pressed
        else {
            if(this.player.sprite.body.velocity.y > 0) {
                if(this.player.sprite.y + this.player.resistant_acceleration.y > game.height - information_bar_height - Math.abs(this.player.sprite.height)/2){
                    this.player.sprite.y = game.height - information_bar_height - Math.abs(this.player.sprite.height)/2;
                    this.player.sprite.body.velocity.y = 0;
                }
                else {
                    this.player.sprite.body.velocity.y -= this.player.move_acceleration.y;
                }
            }
            else if(this.player.sprite.body.velocity.y < 0) {
                if(this.player.sprite.y - this.player.resistant_acceleration.y < Math.abs(this.player.sprite.height)/2){
                    this.player.sprite.y = Math.abs(this.player.sprite.height)/2;
                    this.player.sprite.body.velocity.y = 0;
                }
                else {
                    this.player.sprite.body.velocity.y += this.player.move_acceleration.y;
                }
            }
            else {
                this.player.sprite.body.velocity.y = 0;
            }
        }
        
        // move left
        if(this.cursor.left.isDown) { 
            if(this.player.sprite.x - this.player.move_acceleration.x < this.player.sprite.width/2) {
                this.player.sprite.x = this.player.sprite.width/2;
                this.player.sprite.body.velocity.x = 0;
            }
            else {
                this.player.sprite.body.velocity.x -= this.player.move_acceleration.x;
            }
            this.player.sprite.animations.play('player_move_left');
        }
        // move right
        else if(this.cursor.right.isDown) { 
            if(this.player.sprite.x + this.player.move_acceleration.x > game.width - this.player.sprite.width/2) {
                this.player.sprite.x = game.width - this.player.sprite.width/2;
                this.player.sprite.body.velocity.x = 0;
            }
            else {
                this.player.sprite.body.velocity.x += this.player.move_acceleration.x;
            }
            this.player.sprite.animations.play('player_move_right');
        }
        // if neither left nor right is pressed
        else {
            if(this.player.sprite.body.velocity.x > 0) {
                if(this.player.sprite.x + this.player.resistant_acceleration.x > game.width - this.player.sprite.width/2) {
                    this.player.sprite.x = game.width - this.player.sprite.width/2;
                    this.player.sprite.body.velocity.x = 0;
                }
                else {
                    this.player.sprite.body.velocity.x -= this.player.move_acceleration.x;
                }
            }
            else if(this.player.sprite.body.velocity.x < 0) {
                if(this.player.sprite.x - this.player.resistant_acceleration.x < this.player.sprite.width/2) {
                    this.player.sprite.x = this.player.sprite.width/2;
                    this.player.sprite.body.velocity.x = 0;
                }
                else {
                    this.player.sprite.body.velocity.x += this.player.move_acceleration.x;
                }
            }
            else {
                this.player.sprite.body.velocity.x = 0;
            }
            this.player.sprite.animations.play('player_move');
        }

        // avoid player move out of world
        if(this.player.sprite.x < this.player.sprite.width/2) {
            this.player.sprite.x = this.player.sprite.width/2;
        }
        else if(this.player.sprite.x > game.width - this.player.sprite.width/2) {
            this.player.sprite.x = game.width - this.player.sprite.width/2;
        }

        if(this.player.sprite.y < Math.abs(this.player.sprite.height)/2) {
            this.player.sprite.y = Math.abs(this.player.sprite.height)/2;
        }
        else if(this.player.sprite.y > game.height - information_bar_height - Math.abs(this.player.sprite.height)/2) {
            this.player.sprite.y = game.height - information_bar_height - Math.abs(this.player.sprite.height)/2;
        }
    },
    */
    
    playerMove: function() {
        // move up
        if(this.cursor.up.isDown) {
            this.player.sprite.body.velocity.y = -this.player.move_velocity.y;
        }
        // move down
        else if(this.cursor.down.isDown) { 
            this.player.sprite.body.velocity.y = this.player.move_velocity.y;
        }
        // if neither up nor down is pressed
        else {
            this.player.sprite.body.velocity.y = 0;
        }
        
        // move left
        if(this.cursor.left.isDown) { 
            this.player.sprite.body.velocity.x = -this.player.move_velocity.x;
            this.player.sprite.animations.play('player_move_left');
        }
        // move right
        else if(this.cursor.right.isDown) { 
            this.player.sprite.body.velocity.x = this.player.move_velocity.x;
            this.player.sprite.animations.play('player_move_right');
        }
        // if neither left nor right is pressed
        else {
            this.player.sprite.body.velocity.x = 0;
            this.player.sprite.animations.play('player_move');
        }

        // avoid player move out of world
        if(this.player.sprite.x < this.player.sprite.width/2) {
            this.player.sprite.x = this.player.sprite.width/2;
        }
        else if(this.player.sprite.x > game.width - this.player.sprite.width/2) {
            this.player.sprite.x = game.width - this.player.sprite.width/2;
        }

        if(this.player.sprite.y < Math.abs(this.player.sprite.height)/2) {
            this.player.sprite.y = Math.abs(this.player.sprite.height)/2;
        }
        else if(this.player.sprite.y > game.height - information_bar_height - Math.abs(this.player.sprite.height)/2) {
            this.player.sprite.y = game.height - information_bar_height - Math.abs(this.player.sprite.height)/2;
        }
    },

    playerAttack: function() {
        // reload the bullet
        if(this.player.attack_counter == this.player.attack_period) {
            this.player.attack_counter = this.player.attack_period;
        }
        else {
            this.player.attack_counter++;
        }
        
        // emit bullet if bullet has reloaded
        if(this.keyboard.isDown(Phaser.KeyCode.SPACEBAR)) {
            var bullet_imageName = this.player.bullet_imageName;
            var bullet_position = {
                x: this.player.sprite.x,
                y: this.player.sprite.y
            };
            var bullet_velocity = this.player.bullet_velocity;
            var bullet_angle = this.player.bullet_angle;
            var bullet_damage = this.player.bullet_damage;
            
            if(this.player.attack_counter == this.player.attack_period) {
                var new_bullet = this.create_new_bullet(bullet_imageName, bullet_position, bullet_velocity, bullet_angle, bullet_damage);
                this.player_bullet_set.push(new_bullet);
                this.player.attack_counter = 0;

                // play bullet shoot sound effect
                var soundEffect_soundEffectName = 'bullet_shoot_sound_effect';
                var soundEffect_volume = game.global.sound_effect_volume;
                var soundEffect_loop = false;

                var new_sound_effect = this.creat_new_sound_effect(soundEffect_soundEffectName, soundEffect_volume, soundEffect_loop);
                new_sound_effect.play();
            }
        }
    },

    playerPowerIncrementUsingTimer: function() {
        this.increasePlayerPower(this.player.power_increment.timer);
    },

    playerPowerIncrementUsingAttack: function() {
        this.increasePlayerPower(this.player.power_increment.attack);
    },

    playerPowerIncrementUsingKill: function() {
        this.increasePlayerPower(this.player.power_increment.kill);
    },

    playerUltimateSkill: function() {
        if(this.player.missile_attack_ongoing_bullet_set.length == 0 && this.player.power == this.player.max_power) {
            if(this.keyboard.isDown(Phaser.KeyCode.SHIFT) && this.enemy_set.length != 0) {
                var enemyAllNotIdle = true;
                for(var i = 0; i < this.enemy_set.length; i++) {
                    if(this.enemy_set[i].idle) {
                        enemyAllNotIdle = false;
                        break;
                    }
                }
                
                if(!enemyAllNotIdle) {
                    for(var i = 0; i < this.player.missile_num; i++) {
                        var new_missile = {
                            missile_imageName: this.player.missile_imageName,
                            missile_position: {
                                x: this.player.sprite.x,
                                y: this.player.sprite.y
                            },
                            missile_velocity: this.player.missile_velocity,
                            missile_angle: this.player.missile_angle,
                            missile_damage: this.player.missile_damage
                        };
        
                        this.player.missile_attack_ongoing_bullet_set.push(new_missile);
                    }
        
                    this.player.missile_attack_ongoing_counter = this.player.missile_emit_interval;
                    
                    this.resetPlayerPower();
                }
            }
        }
    },

    playerMissileAttackOngoing: function() {
       if(this.player.missile_attack_ongoing_bullet_set.length != 0) {
            if(this.player.missile_attack_ongoing_counter == this.player.missile_emit_interval) {
                var processing_missile = this.player.missile_attack_ongoing_bullet_set.shift();
                processing_missile.missile_position = {
                    x: this.player.sprite.x,
                    y: this.player.sprite.y
                }
    
                var new_missile = this.create_new_missile(processing_missile.missile_imageName, processing_missile.missile_position, processing_missile.missile_velocity, processing_missile.missile_angle, processing_missile.missile_damage);
                
                // play missile sound effect
                var soundEffect_soundEffectName = 'missile_shoot_sound_effect';
                var soundEffect_volume = game.global.sound_effect_volume;
                var soundEffect_loop = false;

                var new_sound_effect = this.creat_new_sound_effect(soundEffect_soundEffectName, soundEffect_volume, soundEffect_loop);
                if(this.enemy_set.length != 0) {
                    new_sound_effect.play();
                }

                this.player_missile_set.push(new_missile);
    
                this.player.missile_attack_ongoing_counter = 0;
            }
            else {
                this.player.missile_attack_ongoing_counter++;
            }
        }
        else {
            this.player.missile_attack_ongoing_counter = 0;
        }
    },

    playerMissileMove: function() {
        for(var i = 0; i < this.player_missile_set.length; i++) {
            var processing_player_missile = this.player_missile_set[i];
            var processing_player_missile_position = {
                x: processing_player_missile.sprite.x,
                y: processing_player_missile.sprite.y
            };

            var nearest_enemy_position = this.search_nearest_enemy_position(processing_player_missile_position, this.enemy_set);
            if(nearest_enemy_position == undefined) {
                processing_player_missile.sprite.destroy();
                this.player_missile_set.splice(i, 1);
            }
            else {
                var nearest_enemy_distance = this.two_point_distance(processing_player_missile_position, nearest_enemy_position);
                var nearest_enemy_unit_vector = {
                    x: (nearest_enemy_position.x - processing_player_missile_position.x) / nearest_enemy_distance,
                    y: (nearest_enemy_position.y - processing_player_missile_position.y) / nearest_enemy_distance
                };

                processing_player_missile.sprite.body.velocity.x = processing_player_missile.velocity * nearest_enemy_unit_vector.x;
                processing_player_missile.sprite.body.velocity.y = processing_player_missile.velocity * nearest_enemy_unit_vector.y;
            }
        }
    },

    playerWin: function() {
        this.pause = true;
        
        // stop player
        this.player.sprite.body.velocity.x = 0;
        this.player.sprite.body.velocity.y = 0;

        // stop bgm
        this.bgm.stop();
        
        // update score
        this.scoreUpdate();

        // start win state
        game.time.events.add(1000, function() {
            game.state.start('win');
        });
    },

    playerDie: function() {
        this.pause = true;
        
        // stop enemy
        for(var i = 0; i < this.enemy_set.length; i++) {
            this.enemy_set[i].sprite.body.velocity.x = 0;
            this.enemy_set[i].sprite.body.velocity.y = 0;
        }

        // stop bgm
        this.bgm.stop();
        
        // update score
        this.scoreUpdate();

        // start lose state
        game.time.events.add(1000, function() {
            game.state.start('lose');
        });
    },

    // update player power
    increasePlayerPower: function(increament) {
        if(this.player.power + increament >= this.player.max_power) {
            this.player.power = this.player.max_power;
        }
        else {
            this.player.power += increament;
        }
        this.player.power_bar.setPercent((this.player.power / this.player.max_power)*100);
    },

    resetPlayerPower: function() {
        this.player.power = 0;
        this.player.power_bar.setPercent((this.player.power / this.player.max_power)*100);
    },

    // create enemy
    create_enemy: function() {
        for(var i = 0; i < this.enemy_create_schedule.length; i++) {
            if(this.enemy_create_schedule[i].timer == this.timer) {
                var processing_schedule = this.enemy_create_schedule[i];
                var num = processing_schedule.num;
                var enemy_create_period = (num == 1) ? 60 : (refresh_frequency / (num-1));
                for(var j = 0; j < num; j++) {
                    if(enemy_create_period * j == this.timer_counter) {
                        var processing_enemy_information = processing_schedule.enemy_information_set[j];
                        var position = processing_enemy_information.position;
                        var Enemy = processing_enemy_information.Enemy;
                        
                        var new_enemy = this.create_new_enemy(position, Enemy);

                        this.enemy_set.push(new_enemy);
                    }
                }
            }
        }
    },

    // create new enemy
    create_new_enemy: function(position, Enemy) {
        var health_bar_config = {
            width: game.cache.getImage(Enemy.enemy_imageName).width*0.15,
            height: 10,
            x: position.x,
            y: -50 - game.cache.getImage(Enemy.enemy_imageName).height*0.15,
            bg: {
                color: '#000000'
            },
            bar: {
                color: '#FF0000'
            },
            animationDuration: 50,
            flipped: false
        };
        var new_enemy = {
            sprite: game.add.sprite(position.x, -50, Enemy.enemy_imageName),
            max_health: Enemy.max_health,
            health: Enemy.max_health,
            health_bar: new HealthBar(game, health_bar_config),
            idle: false,
            start_position: {x: position.x, y:position.y},
            move_counter: Enemy.move_period,
            move_period: Enemy.move_period,
            move_velocity: Enemy.move_velocity,
            move_angle_set:Enemy.move_angle_set,
            attack_counter: Enemy.attack_period,
            attack_period: Enemy.attack_period,
            attack_ongoing_counter: 0,
            attack_ongoing_bullet_set: [],
            bullet_imageName: Enemy.bullet_imageName,
            bullet_num: Enemy.bullet_num,
            bullet_emit_interval: Enemy.bullet_emit_interval,
            bullet_velocity: Enemy.bullet_velocity,
            bullet_angle_set: Enemy.bullet_angle_set,
            bullet_damage: Enemy.bullet_damage,
            killed_score: Enemy.killed_score
        };
        
        new_enemy.sprite.scale.setTo(0.15, 0.15);
        new_enemy.sprite.scale.y *= -1;
        new_enemy.sprite.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(new_enemy.sprite);
        
        return new_enemy;
    },

    // enemy action
    enemyMove: function(enemy) {
        if(enemy.idle) {
            if(enemy.move_counter == enemy.move_period) {
                var move_angle = enemy.move_angle_set[Math.floor(Math.random() * enemy.move_angle_set.length)];
                var move_velocity = {
                    x: enemy.move_velocity * Math.cos(move_angle*2*Math.PI/360),
                    y: enemy.move_velocity * Math.sin(move_angle*2*Math.PI/360)
                };
    
                enemy.sprite.body.velocity.x = move_velocity.x;
                enemy.sprite.body.velocity.y = move_velocity.y;
    
                enemy.move_counter = 0;
            }
            else {
                enemy.move_counter++;
            }

            // avoid enemy move out of world
            if(enemy.sprite.x < enemy.sprite.width/2) {
                enemy.sprite.x = enemy.sprite.width/2;
                enemy.sprite.body.velocity.x = 0;
            }
            else if(enemy.sprite.x > game.width - enemy.sprite.width/2) {
                enemy.sprite.x = game.width - enemy.sprite.width/2;
                enemy.sprite.body.velocity.x = 0;
            }

            if(enemy.sprite.y < Math.abs(enemy.sprite.height)/2) {
                enemy.sprite.y = Math.abs(enemy.sprite.height)/2;
                enemy.sprite.body.velocity.y = 0;
            }
            else if(enemy.sprite.y > game.height/2 - Math.abs(enemy.sprite.height)/2) {
                enemy.sprite.y = game.height/2 - Math.abs(enemy.sprite.height)/2;
                enemy.sprite.body.velocity.y = 0;
            }

            // move health bar
            enemy.health_bar.setPosition(enemy.sprite.x, enemy.sprite.y - Math.abs(enemy.sprite.height/2) - 10);
        }
        else {
            var position_1 = {x: enemy.sprite.x, y: enemy.sprite.y};
            var position_2 = enemy.start_position;
            if(this.two_point_distance(position_1, position_2) < enemy_idle_distance) {
                enemy.idle = true;
            }
            else {
                var distance = {
                    x: enemy.start_position.x - enemy.sprite.x,
                    y: enemy.start_position.y - enemy.sprite.y
                };
                
                enemy.sprite.body.velocity.x = distance.x;
                enemy.sprite.body.velocity.y = distance.y;
            }
        }
    },

    enemyAttack: function(enemy) {
        if(enemy.idle) {
            if(enemy.attack_counter == enemy.attack_period) {
                for(var i = 0; i < enemy.bullet_num; i++) {
                    var new_bullet_set = [];
                    for(var j = 0; j < enemy.bullet_angle_set.length; j++) {
                        var new_bullet = {
                            bullet_imageName: enemy.bullet_imageName,
                            bullet_position: {
                                x: enemy.sprite.x,
                                y: enemy.sprite.y
                            },
                            bullet_velocity: enemy.bullet_velocity,
                            bullet_angle: enemy.bullet_angle_set[j],
                            bullet_damage: enemy.bullet_damage
                        };
    
                        new_bullet_set.push(new_bullet);
                    }
    
                    enemy.attack_ongoing_bullet_set.push(new_bullet_set);
                }
    
                enemy.attack_ongoing_counter = enemy.bullet_emit_interval;
                enemy.attack_counter = 0;
            }
            else {
                enemy.attack_counter++;
            }
        }
    },

    enemyAttackOngoing: function(enemy) {
        if(enemy.idle) {
            if(enemy.attack_ongoing_bullet_set.length != 0) {
                if(enemy.attack_ongoing_counter == enemy.bullet_emit_interval) {
                    var processing_bullet_set = enemy.attack_ongoing_bullet_set.shift();
                    for(var i = 0; i < processing_bullet_set.length; i++) {
                        var processing_bullet = processing_bullet_set[i];
                        processing_bullet.bullet_position = {
                            x: enemy.sprite.x,
                            y: enemy.sprite.y
                        }
                        
                        if(this.enemy_bullet_set.length <= max_enemy_bullet_num) {
                            var new_bullet = this.create_new_bullet(processing_bullet.bullet_imageName, processing_bullet.bullet_position, processing_bullet.bullet_velocity, processing_bullet.bullet_angle, processing_bullet.bullet_damage);
    
                            this.enemy_bullet_set.push(new_bullet);
                        }
                    }
    
                    enemy.attack_ongoing_counter = 0;
                }
                else {
                    enemy.attack_ongoing_counter++;
                }
            }
            else {
                enemy.attack_ongoing_counter = 0;
            }
        }
    },

    // create new bullet
    create_new_bullet: function(imageName, position, velocity, angle, damage) {
        var new_bullet = {
            sprite: game.add.sprite(position.x, position.y, imageName),
            damage: damage
        };
        new_bullet.sprite.scale.setTo(1.0, 1.0);
        new_bullet.sprite.anchor.setTo(0.5, 0.5);

        game.physics.arcade.enable(new_bullet.sprite);

        new_bullet.sprite.body.velocity.x = velocity * Math.cos(angle*2*Math.PI/360);
        new_bullet.sprite.body.velocity.y = velocity * Math.sin(angle*2*Math.PI/360);

        return new_bullet;
    },

    // create new missile
    create_new_missile: function(imageName, position, velocity, angle, damage) {
        var new_missile = {
            sprite: game.add.sprite(position.x, position.y, imageName),
            velocity: velocity,
            damage: damage,
        }

        new_missile.sprite.scale.setTo(0.05, 0.05);
        new_missile.sprite.anchor.setTo(0.5, 0.5);

        game.physics.arcade.enable(new_missile.sprite);

        new_missile.sprite.body.velocity.x = velocity * Math.cos(angle*2*Math.PI/360);
        new_missile.sprite.body.velocity.y = velocity * Math.sin(angle*2*Math.PI/360);

        return new_missile;
    },

    // score action
    scoreIncrease: function(increament) {
        game.global.score += increament;
        this.score_label.text = ('score : ' + game.global.score);
    },

    scoreReset: function() {
        game.global.score = 0;
        this.score_label.text = ('score : ' + game.global.score);
    },

    scoreUpdate: function() {
        // update local database
        var userFound = false;
        for(var i = 0; i < game.global.record_list.length; i++) {
            if(game.global.record_list[i].user_name == game.global.user_name) {
                userFound = true;
                if(game.global.record_list[i].score < game.global.score) {
                    game.global.record_list[i].score = game.global.score;
                }
                break;
            }
        }

        if(!userFound) {
            game.global.record_list.push({
                user_name: game.global.user_name,
                score: game.global.score
            });
        }

        // sort record list
        game.global.record_list.sort(function(object_1, object_2) {
            return ((object_1.score == object_2.score) ? (object_1.user_name.localeCompare(object_2.user_name)) : (object_2.score - object_1.score));
        });

        // update firebase database
        firebase.database().ref('record_list').once('value').then(function(snapshot) {
            if(snapshot.hasChild(game.global.user_name)) {
                if(snapshot.child(game.global.user_name).val().score < game.global.score) {
                    snapshot.child(game.global.user_name).ref.update({
                        score: game.global.score
                    });
                }
            }
            else {
                snapshot.child(game.global.user_name).ref.set({
                    score: game.global.score
                });
            }
        });
    },

    // detect bullet attack
    detect_bullet_attack: function() {
        // player's bullet v.s enemy
        for(var i = 0; i < this.player_bullet_set.length; i++) {
            var processing_player_bullet = this.player_bullet_set[i];
            for(var j = 0; j < this.enemy_set.length; j++) {
                var processing_enemy = this.enemy_set[j];
                if(!(processing_player_bullet.sprite.inWorld)) {
                    processing_player_bullet.sprite.destroy();
                    this.player_bullet_set.splice(i, 1);
                    break;
                }
                else {
                    if(processing_enemy.idle) {
                        var object_1 = {
                            x: processing_player_bullet.sprite.x,
                            y: processing_player_bullet.sprite.y,
                            r: Math.max(Math.abs(processing_player_bullet.sprite.width/2), Math.abs(processing_player_bullet.sprite.height)/2)
                        };
                        var object_2 = {
                            x: processing_enemy.sprite.x,
                            y: processing_enemy.sprite.y,
                            r: Math.max(Math.abs(processing_enemy.sprite.width/2), Math.abs(processing_enemy.sprite.height)/2)
                        };
                        var enemy_killed_score = processing_enemy.killed_score;
    
                        if(this.circle_overlap(object_1, object_2)) {
                            // increase player's power using attack
                            this.playerPowerIncrementUsingAttack();
                            
                            this.player_bullet_set.splice(i, 1);
                            if(this.object_killed(processing_player_bullet, processing_enemy)) {
                                this.enemy_set.splice(j, 1);
    
                                // increase player's power using kill
                                this.playerPowerIncrementUsingKill();
    
                                // increase score
                                this.scoreIncrease(enemy_killed_score);
    
                                // if all enemy has created and all enemy died
                                if(this.timer > this.enemy_create_schedule[this.enemy_create_schedule.length-1].timer) {
                                    if(this.enemy_set.length == 0) {
                                        this.playerWin();
                                    }
                                }
                            }
    
                            break;
                        }
                    }
                }
            }
        }

        // player's missile v.s enemy
        for(var i = 0; i < this.player_missile_set.length; i++) {
            var processing_player_missile = this.player_missile_set[i];
            for(var j = 0; j < this.enemy_set.length; j++) {
                var processing_enemy = this.enemy_set[j];
                if(!(processing_player_missile.sprite.inWorld)) {
                    processing_player_missile.sprite.destroy();
                    this.player_missile_set.splice(i, 1);
                    break;
                }
                else {
                    if(processing_enemy.idle) {
                        var object_1 = {
                            x: processing_player_missile.sprite.x,
                            y: processing_player_missile.sprite.y,
                            r: Math.max(Math.abs(processing_player_missile.sprite.width/2), Math.abs(processing_player_missile.sprite.height)/2)
                        };
                        var object_2 = {
                            x: processing_enemy.sprite.x,
                            y: processing_enemy.sprite.y,
                            r: Math.max(Math.abs(processing_enemy.sprite.width/2), Math.abs(processing_enemy.sprite.height)/2)
                        };
                        var enemy_killed_score = processing_enemy.killed_score;
    
                        if(this.circle_overlap(object_1, object_2)) {
                            // increase player's power using attack
                            this.playerPowerIncrementUsingAttack();
                            
                            this.player_missile_set.splice(i, 1);
                            if(this.object_killed(processing_player_missile, processing_enemy)) {
                                this.enemy_set.splice(j, 1);
    
                                // increase player's power using kill
                                this.playerPowerIncrementUsingKill();
    
                                // increase score
                                this.scoreIncrease(enemy_killed_score);
    
                                // if all enemy has created and all enemy died
                                if(this.timer > this.enemy_create_schedule[this.enemy_create_schedule.length-1].timer) {
                                    if(this.enemy_set.length == 0) {
                                        this.playerWin();
                                    }
                                }
                            }
    
                            break;
                        }
                    }
                }
            }
        }

        // enemy's bullet v.s player
        for(var i = 0; i < this.enemy_bullet_set.length; i++) {
            var processing_enemy_bullet = this.enemy_bullet_set[i];
            if(!(processing_enemy_bullet.sprite.inWorld)) {
                processing_enemy_bullet.sprite.destroy();
                this.enemy_bullet_set.splice(i, 1);
                break;
            }
            else {
                var object_1 = {
                    x: processing_enemy_bullet.sprite.x,
                    y: processing_enemy_bullet.sprite.y,
                    r: Math.max(Math.abs(processing_enemy_bullet.sprite.width)/2, Math.abs(processing_enemy_bullet.sprite.height)/2)
                };
                var object_2 = {
                    x: this.player.sprite.x,
                    y: this.player.sprite.y,
                    r: Math.max(Math.abs(this.player.sprite.width)/2, Math.abs(this.player.sprite.height)/2)
                };

                if(this.circle_overlap(object_1, object_2)) {
                    this.enemy_bullet_set.splice(i, 1);
                    if(this.object_killed(processing_enemy_bullet, this.player)) {
                        this.playerDie();
                    }
                }
            }
        }
    },

    // object killed
    object_killed: function(bullet, object) {
        bullet.sprite.destroy();
        if(object.health - bullet.damage <= 0) {
            object.health_bar.kill()
            object.sprite.destroy();

            // play explode emitter
            var emitter_position = {
                x: object.sprite.x,
                y: object.sprite.y
            };
            var emitter_maxParticles = 15;
            var emitter_imageName = 'explode_emitter';
            var emitter_speed = {
                x: {
                    min: -150,
                    max: 150
                },
                y: {
                    min: -150,
                    max: 150
                }
            };
            var emitter_scale = {
                startX: 0.1,
                endX: 0,
                startY: 0.1,
                endY: 0,
                duration: 800
            }
            var emitter_start = {
                explode: true,
                lifespan: 800,
                frequency: null,
                quantity: 15
            };
            
            var new_emitter = this.create_new_emitter(emitter_position, emitter_maxParticles, emitter_imageName, emitter_speed, emitter_scale);
            new_emitter.start(emitter_start.explode, emitter_start.lifespan, emitter_start.frequency, emitter_start.quantity);

            // play explode sound effect
            var soundEffect_soundEffectName = 'explode_sound_effect';
            var soundEffect_volume = game.global.sound_effect_volume;
            var soundEffect_loop = false;

            var new_sound_effect = this.creat_new_sound_effect(soundEffect_soundEffectName, soundEffect_volume, soundEffect_loop);
            new_sound_effect.play();

            return true;
        }
        else {
            object.health -= bullet.damage;
            object.health_bar.setPercent((object.health / object.max_health)*100);
        }
    },

    // create new emitter
    create_new_emitter : function(position, maxParticles, imageName, speed, scale) {
        var new_emitter = game.add.emitter(position.x, position.y, maxParticles);
        new_emitter.makeParticles(imageName);
        new_emitter.setXSpeed(speed.x.min, speed.x.max);
        new_emitter.setYSpeed(speed.y.min, speed.y.max);
        new_emitter.setScale(scale.startX, scale.endX, scale.startY, scale.endY, scale.duration);
        new_emitter.gravity = 0;

        return new_emitter;
    },

    // creat new sound effect
    creat_new_sound_effect: function(soundEffectName, volume, loop) {
        var new_sound_effect = game.add.audio(soundEffectName);
        new_sound_effect.volume = volume;
        new_sound_effect.loop = loop;
        
        return new_sound_effect;
    }
}